define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for flxHamburger **/
    AS_FlexContainer_cf410d81d2b746f79996bb3b52aefb94: function AS_FlexContainer_cf410d81d2b746f79996bb3b52aefb94(eventobject, x, y) {
        var self = this;

        function ___ide_onTouchStart_dba855eddb5c4fee8e2b8dc1a89ca1c2_Callback() {}
        function ___ide_onTouchStart_e0a1daf02b5149a892afc7803d65d96c_Callback() {}
        self.view.Hamburger.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": ___ide_onTouchStart_e0a1daf02b5149a892afc7803d65d96c_Callback
        });
        self.view.flxContent.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "80%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": ___ide_onTouchStart_dba855eddb5c4fee8e2b8dc1a89ca1c2_Callback
        });
    },
    /** onPinClick defined for placesMap **/
    AS_Map_c5289b1e752f4bddb61db871922bb9a9: function AS_Map_c5289b1e752f4bddb61db871922bb9a9(eventobject, location) {
        var self = this;
        return self.pinClicked.call(this, eventobject);
    },
    /** onSelection defined for placesMap **/
    AS_Map_bc022558a2cd4db7a32b202b5d9f4690: function AS_Map_bc022558a2cd4db7a32b202b5d9f4690(eventobject, location) {
        var self = this;
        return self.calloutClicked.call(this, eventobject, location);
    },
    /** onMapLoaded defined for placesMap **/
    AS_Map_b488b7061117472aa85d3c4d07f60ff9: function AS_Map_b488b7061117472aa85d3c4d07f60ff9(eventobject) {
        var self = this;
        return self.mapData.call(this, eventobject);
    },
    /** onTouchStart defined for flxPullUp **/
    AS_FlexContainer_cd6afb12d01c434aa66f0e5e73160e32: function AS_FlexContainer_cd6afb12d01c434aa66f0e5e73160e32(eventobject, x, y) {
        var self = this;

        function MOVE_ACTION____fcff9906067a4de3a4e356a1d8da2f7b_Callback() {}
        self.view.flxSegData.animate(
        kony.ui.createAnimation({
            "100": {
                "top": "15%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": MOVE_ACTION____fcff9906067a4de3a4e356a1d8da2f7b_Callback
        });
        self.view.flxPullUp.isVisible = false;
        self.view.flxScrollDown.isVisible = true;
    },
    /** onTouchStart defined for flxScrollDown **/
    AS_FlexContainer_b9bb97559fa04bbeab42283bfd12ffb4: function AS_FlexContainer_b9bb97559fa04bbeab42283bfd12ffb4(eventobject, x, y) {
        var self = this;

        function MOVE_ACTION____db64741e6d6e4d7d8336bef1ed2efee2_Callback() {}
        self.view.flxScrollDown.isVisible = false;
        self.view.flxSegData.animate(
        kony.ui.createAnimation({
            "100": {
                "top": "68%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": MOVE_ACTION____db64741e6d6e4d7d8336bef1ed2efee2_Callback
        });
        self.view.flxPullUp.isVisible = true;
    },
    /** onRowClick defined for segDataResults **/
    AS_Segment_h1b39a5452154f06ad9edf06c4a9a8b0: function AS_Segment_h1b39a5452154f06ad9edf06c4a9a8b0(eventobject, sectionNumber, rowNumber) {
        var self = this;
        var ntf = new kony.mvc.Navigation("ServiceContent/frmLogCommplain");
        ntf.navigate({
            "segDataResults_selectedRowItems": self.view.segDataResults.selectedRowItems,
            "_meta_": {
                "eventName": "onRowClick",
                "widgetId": "segDataResults"
            }
        });
    },
    /** onTouchStart defined for flxOverlay **/
    AS_FlexContainer_d6bc386b99754e4da23cffd4d16a3ed5: function AS_FlexContainer_d6bc386b99754e4da23cffd4d16a3ed5(eventobject, x, y) {
        var self = this;

        function MOVE_ACTION____b7ae9be1c83749a59555a9a0b2f0fe83_Callback() {}
        function MOVE_ACTION____j5ce92052e48422c8e13f57d03fbdf1a_Callback() {}
        self.view.Hamburger.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": MOVE_ACTION____j5ce92052e48422c8e13f57d03fbdf1a_Callback
        });
        self.view.flxContent.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": MOVE_ACTION____b7ae9be1c83749a59555a9a0b2f0fe83_Callback
        });
    }
});