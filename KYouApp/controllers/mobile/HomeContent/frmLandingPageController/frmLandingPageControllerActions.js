define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for flxHamburger **/
    AS_FlexContainer_d11833b8c277494b9b515d6eebbecbfd: function AS_FlexContainer_d11833b8c277494b9b515d6eebbecbfd(eventobject, x, y) {
        var self = this;

        function MOVE_ACTION____c8bb5e127c91423cac1d9ea854dcdf1c_Callback() {}
        function MOVE_ACTION____gf475aaeccc645b1a365df9f7605817e_Callback() {}
        self.view.Hamburger.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": MOVE_ACTION____gf475aaeccc645b1a365df9f7605817e_Callback
        });
        self.view.flxContent.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "80%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": MOVE_ACTION____c8bb5e127c91423cac1d9ea854dcdf1c_Callback
        });
    },
    /** onTouchStart defined for flxHospitals **/
    AS_FlexContainer_bd8eac923f6a42d7a3051a573d26fdcc: function AS_FlexContainer_bd8eac923f6a42d7a3051a573d26fdcc(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("HomeContent/frmSearch");
        ntf.navigate();
    },
    /** onTouchStart defined for flxOverlay **/
    AS_FlexContainer_be7f4f7c756d424da6265c513389bc90: function AS_FlexContainer_be7f4f7c756d424da6265c513389bc90(eventobject, x, y) {
        var self = this;

        function MOVE_ACTION____ed5ec2a801e8409abefdf15af088b46f_Callback() {}
        function MOVE_ACTION____gd2b3436df884135994789999cd155f7_Callback() {}
        self.view.flxContent.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": MOVE_ACTION____gd2b3436df884135994789999cd155f7_Callback
        });
        self.view.Hamburger.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": MOVE_ACTION____ed5ec2a801e8409abefdf15af088b46f_Callback
        });
    }
});