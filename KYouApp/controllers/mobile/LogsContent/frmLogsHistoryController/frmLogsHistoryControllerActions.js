define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for flxHamburger **/
    AS_FlexContainer_e88c0463d1f3449c903ebb7bf1099bda: function AS_FlexContainer_e88c0463d1f3449c903ebb7bf1099bda(eventobject, x, y) {
        var self = this;

        function ___ide_onTouchStart_ee364ba62848482c85832342f9c42b1c_Callback() {}
        function ___ide_onTouchStart_aac0b45fcf9f41fb91f175f51d035f81_Callback() {}
        self.view.Hamburger.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": ___ide_onTouchStart_aac0b45fcf9f41fb91f175f51d035f81_Callback
        });
        self.view.flxContent.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "80%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": "1",
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": ___ide_onTouchStart_ee364ba62848482c85832342f9c42b1c_Callback
        });
    },
    /** onTouchStart defined for flxOverlay **/
    AS_FlexContainer_fb2ffaaefc7442ba86640654fa1dc86e: function AS_FlexContainer_fb2ffaaefc7442ba86640654fa1dc86e(eventobject, x, y) {
        var self = this;

        function MOVE_ACTION____d82791eeee514aa398cca5e58ef05ef9_Callback() {}
        function MOVE_ACTION____b4088329e8274a2da9b462879134dd0b_Callback() {}
        self.view.flxContent.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": MOVE_ACTION____b4088329e8274a2da9b462879134dd0b_Callback
        });
        self.view.Hamburger.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": MOVE_ACTION____d82791eeee514aa398cca5e58ef05ef9_Callback
        });
    }
});