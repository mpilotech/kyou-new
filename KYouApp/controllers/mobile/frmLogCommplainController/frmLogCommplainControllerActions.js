define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** recordSuccessCallback defined for audio **/
    AS_UWI_c91a216a1af5453e97c5fe12d2bc7b47: function AS_UWI_c91a216a1af5453e97c5fe12d2bc7b47() {
        var self = this;
        return self.OnSuccessCallBack.call(this);
    },
    /** recordErrorCallback defined for audio **/
    AS_UWI_c9836e44be38462cb934c8abdd09fbc7: function AS_UWI_c9836e44be38462cb934c8abdd09fbc7(error) {
        var self = this;
        return self.OnErrorCallBack.call(this);
    },
    /** onTouchStart defined for visibleButton **/
    AS_Button_i8f59d21b04e4924932fd350855f37e4: function AS_Button_i8f59d21b04e4924932fd350855f37e4(eventobject, x, y) {
        var self = this;

        function ____i57db6eb8bf84650a018afb170ed3984_Callback() {
            self.view.visibleButton.isVisible = false;
            self.view.invisibleButton.isVisible = true;
        }
        undefined.isVisible = false;
        undefined.isVisible = false;
        self.view.flxContentMain.isVisible = false;
        undefined.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "duration": 0.25,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ____i57db6eb8bf84650a018afb170ed3984_Callback
        });
    },
    /** onClick defined for flxTextTab **/
    AS_FlexContainer_f96e71f1900349a299406e37a946b34e: function AS_FlexContainer_f96e71f1900349a299406e37a946b34e(eventobject) {
        var self = this;

        function ___ide_onClick_a1bf40a39d004ff187865c6429f3624e_Callback() {}
        self.view.flxColour1.isVisible = true;
        self.view.flxColour2.isVisible = false;
        self.view.flxColour3.opacity = "0";
        self.view.flxTextAudioSurveyContent.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "direction": kony.anim.DIRECTION_ALTERNATE,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_a1bf40a39d004ff187865c6429f3624e_Callback
        });
    },
    /** onClick defined for flxAudioTab **/
    AS_FlexContainer_hb5389f28f54490083a86d6049f707f9: function AS_FlexContainer_hb5389f28f54490083a86d6049f707f9(eventobject) {
        var self = this;

        function ___ide_onClick_d3d0c7bad2d4446f904ab5d5ee5fe29a_Callback() {}
        self.view.flxColour1.isVisible = false;
        self.view.flxColour2.isVisible = true;
        self.view.flxColour3.opacity = "0";
        self.view.flxTextAudioSurveyContent.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "direction": kony.anim.DIRECTION_ALTERNATE,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_d3d0c7bad2d4446f904ab5d5ee5fe29a_Callback
        });
    },
    /** onClick defined for flxSurveyTab **/
    AS_FlexContainer_b21bfdfb8d7444e8b568d42ed539c1dd: function AS_FlexContainer_b21bfdfb8d7444e8b568d42ed539c1dd(eventobject) {
        var self = this;
    },
    /** onClick defined for flxDeleteAttachment **/
    AS_FlexContainer_e95a19f330ba4cc5b125cb6f579a344a: function AS_FlexContainer_e95a19f330ba4cc5b125cb6f579a344a(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_e5e77ecee253436da7d8df11d6d54ddd_True() {
            self.onAttachmentDelete.call(this);
            self.view.imgImageConfiem.isVisible = false;
            self.view.imgVidConfirm.isVisible = false;
            self.view.flxDeleteAttachment.isVisible = false;
            self.view.lblAttachConfirm.right = "35%";
        }
        function SHOW_ALERT_ide_onClick_e5e77ecee253436da7d8df11d6d54ddd_False() {}
        function SHOW_ALERT_ide_onClick_e5e77ecee253436da7d8df11d6d54ddd_Callback(response) {
            if (response === true) {
                SHOW_ALERT_ide_onClick_e5e77ecee253436da7d8df11d6d54ddd_True();
            } else {
                SHOW_ALERT_ide_onClick_e5e77ecee253436da7d8df11d6d54ddd_False();
            }
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "alertTitle": "DELETE ATTACHMENT",
            "yesLabel": "YES",
            "noLabel": "NO",
            "alertIcon": null,
            "message": "Your attachment will be removed. Do you want to continue?",
            "alertHandler": SHOW_ALERT_ide_onClick_e5e77ecee253436da7d8df11d6d54ddd_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
        });
    },
    /** onClick defined for flxAttachFile **/
    AS_FlexContainer_f5b34b71416a460b9ddc010bd30f0ce5: function AS_FlexContainer_f5b34b71416a460b9ddc010bd30f0ce5(eventobject) {
        var self = this;

        function ___ide_onClick_fd16397026264f448ead3e5193f35387_Callback() {}
        self.view.flxAttachImage.isVisible = true;
        self.view.flxAttachImage.animate(
        kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "top": "0%"
            }
        }), {
            "delay": 0,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_fd16397026264f448ead3e5193f35387_Callback
        });
    },
    /** onClick defined for flxDelete **/
    AS_FlexContainer_h2a6d022426d4709b530b6a83e430bfb: function AS_FlexContainer_h2a6d022426d4709b530b6a83e430bfb(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_bf15fedc866d438982c8fe455a70e23f_True() {
            self.onTextAreaCancel.call(this);
        }
        function SHOW_ALERT_ide_onClick_bf15fedc866d438982c8fe455a70e23f_False() {}
        function SHOW_ALERT_ide_onClick_bf15fedc866d438982c8fe455a70e23f_Callback(response) {
            if (response === true) {
                SHOW_ALERT_ide_onClick_bf15fedc866d438982c8fe455a70e23f_True();
            } else {
                SHOW_ALERT_ide_onClick_bf15fedc866d438982c8fe455a70e23f_False();
            }
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "alertTitle": "Cencel Message",
            "yesLabel": "Yes",
            "noLabel": "No",
            "message": "Your message will be canceled!!...Complete process?",
            "alertHandler": SHOW_ALERT_ide_onClick_bf15fedc866d438982c8fe455a70e23f_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
        });
    },
    /** onClick defined for flxSendText **/
    AS_FlexContainer_a4d069b1eb53405582cad1c797e97184: function AS_FlexContainer_a4d069b1eb53405582cad1c797e97184(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_a9174231e6b74504ae8983ee0799f5f6_True() {}
        function SHOW_ALERT_ide_onClick_a9174231e6b74504ae8983ee0799f5f6_Callback() {
            SHOW_ALERT_ide_onClick_a9174231e6b74504ae8983ee0799f5f6_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_INFO,
            "alertTitle": "Message Sent!!",
            "yesLabel": "OK",
            "message": "Your text message has been received. We will attempt to resolve it ASAP. Thank you.",
            "alertHandler": SHOW_ALERT_ide_onClick_a9174231e6b74504ae8983ee0799f5f6_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
        });
    },
    /** onClick defined for flxRecordIcon **/
    AS_FlexContainer_a5ca18deec944e54a60296bbf059ae71: function AS_FlexContainer_a5ca18deec944e54a60296bbf059ae71(eventobject) {
        var self = this;
        return self.OnStartRecord.call(this);
    },
    /** onClick defined for flxReplayContent **/
    AS_FlexContainer_f517a72473b64660b4ca4703b1f4efba: function AS_FlexContainer_f517a72473b64660b4ca4703b1f4efba(eventobject) {
        var self = this;
        return self.OnIsRecordExist.call(this);
    },
    /** onClick defined for flxCancelContent **/
    AS_FlexContainer_ceb2e68d726545368513efd4bcb96ce1: function AS_FlexContainer_ceb2e68d726545368513efd4bcb96ce1(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_dd949d1961064731aa559b9a736b9d82_True() {}
        self.OnStopRecord.call(this);

        function SHOW_ALERT_ide_onClick_dd949d1961064731aa559b9a736b9d82_Callback() {
            SHOW_ALERT_ide_onClick_dd949d1961064731aa559b9a736b9d82_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_INFO,
            "alertTitle": "Audio Message Canceled!!",
            "yesLabel": "OK",
            "message": "Your audio has stopped. You can do another record again.",
            "alertHandler": SHOW_ALERT_ide_onClick_dd949d1961064731aa559b9a736b9d82_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
        });
    },
    /** onClick defined for flxAttachContent **/
    AS_FlexContainer_b586b9eb562e4fffbdb4ef284a87482d: function AS_FlexContainer_b586b9eb562e4fffbdb4ef284a87482d(eventobject) {
        var self = this;

        function ___ide_onClick_ef6d92bf14bd444184999ba16b19de27_Callback() {}
        self.view.flxAttachImageForAudio.isVisible = true;
        self.view.flxAttachImageForAudio.animate(
        kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "top": "0%"
            }
        }), {
            "delay": 0,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_ef6d92bf14bd444184999ba16b19de27_Callback
        });
    },
    /** onClick defined for flxSendContent **/
    AS_FlexContainer_db90026b43ed4d198c9db6a01cae43a5: function AS_FlexContainer_db90026b43ed4d198c9db6a01cae43a5(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_g22860f429c947c1a9fc7c8299f3967b_True() {}
        self.OnRecordShare.call(this);

        function SHOW_ALERT_ide_onClick_g22860f429c947c1a9fc7c8299f3967b_Callback() {
            SHOW_ALERT_ide_onClick_g22860f429c947c1a9fc7c8299f3967b_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_INFO,
            "alertTitle": "Audio Sent!!",
            "yesLabel": "OK ... Got it",
            "message": "Your audio message has been received. We will attempt your query ASAP. Thank you.",
            "alertHandler": SHOW_ALERT_ide_onClick_g22860f429c947c1a9fc7c8299f3967b_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
        });
    },
    /** onClick defined for flxDeleteAttached **/
    AS_FlexContainer_f54ada8888804400aa9d9616848d8da5: function AS_FlexContainer_f54ada8888804400aa9d9616848d8da5(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_i33e877d336149d28305ce628b6a08f9_True() {
            self.onAttachmentDeletes.call(this);
            self.view.imgImageConfiemation.isVisible = false;
            self.view.imgVidConfirmation.isVisible = false;
            self.view.flxDeleteAttached.isVisible = false;
            self.view.lblAttachConfirmation.right = "35%";
        }
        function SHOW_ALERT_ide_onClick_i33e877d336149d28305ce628b6a08f9_False() {}
        function SHOW_ALERT_ide_onClick_i33e877d336149d28305ce628b6a08f9_Callback(response) {
            if (response === true) {
                SHOW_ALERT_ide_onClick_i33e877d336149d28305ce628b6a08f9_True();
            } else {
                SHOW_ALERT_ide_onClick_i33e877d336149d28305ce628b6a08f9_False();
            }
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "alertTitle": "DELETE ATTACHMENT",
            "yesLabel": "YES",
            "noLabel": "NO",
            "alertIcon": null,
            "message": "Your attachment will be removed. Do you want to continue?",
            "alertHandler": SHOW_ALERT_ide_onClick_i33e877d336149d28305ce628b6a08f9_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
        });
    },
    /** onCapture defined for cameraID **/
    AS_Camera_e9afe3c80d564b29a8ad6e54bccaca6a: function AS_Camera_e9afe3c80d564b29a8ad6e54bccaca6a(eventobject) {
        var self = this;
        self.assignToImage.call(this, eventobject);
        self.view.flxImageUploaded.isVisible = true;
    },
    /** onCapture defined for CameraVidCapture **/
    AS_Camera_cd180eba4a7946efb064e1d081d05b0f: function AS_Camera_cd180eba4a7946efb064e1d081d05b0f(eventobject) {
        var self = this;
        self.assignToVideo.call(this, eventobject);
        self.view.flxVidUploaded.isVisible = true;
    },
    /** onClick defined for flxCameraVid **/
    AS_FlexContainer_eef7cf88251e43f18509292ccbc460a4: function AS_FlexContainer_eef7cf88251e43f18509292ccbc460a4(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_cfc41b7e9e4249abb6343a7ff0d311eb_True() {
            var ntf = new kony.mvc.Navigation("frmHomeContent");
            ntf.navigate();
        }
        function SHOW_ALERT_ide_onClick_cfc41b7e9e4249abb6343a7ff0d311eb_False() {}
        function SHOW_ALERT_ide_onClick_cfc41b7e9e4249abb6343a7ff0d311eb_Callback(response) {
            if (response === true) {
                SHOW_ALERT_ide_onClick_cfc41b7e9e4249abb6343a7ff0d311eb_True();
            } else {
                SHOW_ALERT_ide_onClick_cfc41b7e9e4249abb6343a7ff0d311eb_False();
            }
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "alertTitle": "Wonderful!!",
            "yesLabel": null,
            "noLabel": null,
            "alertIcon": null,
            "message": "Your file has been uploaded successfully",
            "alertHandler": SHOW_ALERT_ide_onClick_cfc41b7e9e4249abb6343a7ff0d311eb_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
    },
    /** onClick defined for flxDeleteUpload **/
    AS_FlexContainer_d864a2019a194d8c9163623af6cfc089: function AS_FlexContainer_d864a2019a194d8c9163623af6cfc089(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_cbf1c9c577e848d0843179e2da8e4de6_True() {
            self.view.flxAttachImage.isVisible = false;
            self.view.flxImageUploaded.isVisible = false;
            var ntf = new kony.mvc.Navigation("frmHomeContent");
            ntf.navigate();
        }
        function SHOW_ALERT_ide_onClick_cbf1c9c577e848d0843179e2da8e4de6_False() {}
        function SHOW_ALERT_ide_onClick_cbf1c9c577e848d0843179e2da8e4de6_Callback(response) {
            if (response === true) {
                SHOW_ALERT_ide_onClick_cbf1c9c577e848d0843179e2da8e4de6_True();
            } else {
                SHOW_ALERT_ide_onClick_cbf1c9c577e848d0843179e2da8e4de6_False();
            }
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "alertTitle": "CANCEL ATTACHMENT",
            "yesLabel": "YES",
            "noLabel": "NO",
            "alertIcon": null,
            "message": "Your Image will be deleted. Would you like to proceed?",
            "alertHandler": SHOW_ALERT_ide_onClick_cbf1c9c577e848d0843179e2da8e4de6_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
        });
    },
    /** onCapture defined for cmrRetake **/
    AS_Camera_cca20402a52f42e282d2b71fd1f4bb4a: function AS_Camera_cca20402a52f42e282d2b71fd1f4bb4a(eventobject) {
        var self = this;
        return self.assignToImage.call(this, eventobject);
    },
    /** onClick defined for flxRetake **/
    AS_FlexContainer_d87dfaa791fc49b5a644b42806cccc16: function AS_FlexContainer_d87dfaa791fc49b5a644b42806cccc16(eventobject) {
        var self = this;

        function ___ide_onClick_dfba198fd32c41d9a3efd3025af015c0_Callback() {}
        self.view.flxAttachFile.animate(
        kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "top": "0%"
            }
        }), {
            "delay": 0,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_dfba198fd32c41d9a3efd3025af015c0_Callback
        });
    },
    /** onClick defined for flxUseImage **/
    AS_FlexContainer_cac7804e0d1047fa9e59dfda97b689d3: function AS_FlexContainer_cac7804e0d1047fa9e59dfda97b689d3(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_a2fdb4a591c9420b9c1898cf198226ed_True() {}
        function SHOW_ALERT_ide_onClick_a2fdb4a591c9420b9c1898cf198226ed_False() {}
        var ntf = new kony.mvc.Navigation("frmHomeContent");
        ntf.navigate({
            "imgCaptured_isVisible": self.view.imgCaptured.isVisible,
            "imgCaptured_base64": self.view.imgCaptured.base64,
            "imgCaptured_src": self.view.imgCaptured.src,
            "_meta_": {
                "eventName": "onClick",
                "widgetId": "flxUseImage"
            }
        });

        function SHOW_ALERT_ide_onClick_a2fdb4a591c9420b9c1898cf198226ed_Callback(response) {
            if (response === true) {
                SHOW_ALERT_ide_onClick_a2fdb4a591c9420b9c1898cf198226ed_True();
            } else {
                SHOW_ALERT_ide_onClick_a2fdb4a591c9420b9c1898cf198226ed_False();
            }
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "alertTitle": "Upload Succeeded!!",
            "yesLabel": "OK...Got it!",
            "message": "The image has been attached to your message.",
            "alertHandler": SHOW_ALERT_ide_onClick_a2fdb4a591c9420b9c1898cf198226ed_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
    },
    /** onClick defined for flxDeleteVid **/
    AS_FlexContainer_dc20bb16ce2b4af490b42c92cfab51f6: function AS_FlexContainer_dc20bb16ce2b4af490b42c92cfab51f6(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_f300d55de69e438baf55a6b4eafcec2a_True() {
            self.onVideoUploadCancel.call(this);
            self.view.flxAttachImage.isVisible = false;
            self.view.flxVidUploaded.isVisible = false;
            self.view.flxImageUploaded.isVisible = false;
            var ntf = new kony.mvc.Navigation("frmHomeContent");
            ntf.navigate();
        }
        function SHOW_ALERT_ide_onClick_f300d55de69e438baf55a6b4eafcec2a_False() {}
        function SHOW_ALERT_ide_onClick_f300d55de69e438baf55a6b4eafcec2a_Callback(response) {
            if (response === true) {
                SHOW_ALERT_ide_onClick_f300d55de69e438baf55a6b4eafcec2a_True();
            } else {
                SHOW_ALERT_ide_onClick_f300d55de69e438baf55a6b4eafcec2a_False();
            }
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "alertTitle": "CANCEL ATTACHMENT",
            "yesLabel": "YES",
            "noLabel": "NO",
            "alertIcon": null,
            "message": "Your Video will be deleted. Would you like to proceed?",
            "alertHandler": SHOW_ALERT_ide_onClick_f300d55de69e438baf55a6b4eafcec2a_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
        });
    },
    /** onCapture defined for cmrVidRetake **/
    AS_Camera_a6d99d4184f5466c999f5b912f07c101: function AS_Camera_a6d99d4184f5466c999f5b912f07c101(eventobject) {
        var self = this;
        return self.assignToVideo.call(this, eventobject);
    },
    /** onClick defined for flxRetakeVid **/
    AS_FlexContainer_aa58fb382dad4b08ae3abd6ce1226593: function AS_FlexContainer_aa58fb382dad4b08ae3abd6ce1226593(eventobject) {
        var self = this;

        function ___ide_onClick_bf0d22a8e8b347a1a4621517c41c4b75_Callback() {}
        self.view.flxAttachFile.animate(
        kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "top": "0%"
            }
        }), {
            "delay": 0,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_bf0d22a8e8b347a1a4621517c41c4b75_Callback
        });
    },
    /** onClick defined for flxUseVid **/
    AS_FlexContainer_ae4c34d8432e4f2ba7e8bb2af2fad5a0: function AS_FlexContainer_ae4c34d8432e4f2ba7e8bb2af2fad5a0(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_db455b1dcefe4e0286316998020b45ce_True() {
            self.view.flxAttachImage.isVisible = false;
            self.view.flxVidUploaded.isVisible = false;
            self.view.imgVidConfirm.isVisible = true;
            self.view.lblAttachConfirm.right = "18%";
            self.view.flxDeleteAttachment.isVisible = true;
        }
        function SHOW_ALERT_ide_onClick_db455b1dcefe4e0286316998020b45ce_Callback() {
            SHOW_ALERT_ide_onClick_db455b1dcefe4e0286316998020b45ce_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_INFO,
            "alertTitle": "Video Attached!!",
            "yesLabel": "OK...Got it!",
            "message": "Your video has been successfully attached.",
            "alertHandler": SHOW_ALERT_ide_onClick_db455b1dcefe4e0286316998020b45ce_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
        });
        self.onVideoFileUpload.call(this, eventobject);
        var ntf = new kony.mvc.Navigation("frmHomeContent");
        ntf.navigate();
    },
    /** onClick defined for flxAttachImage **/
    AS_FlexContainer_a799c7b8915340ec8c7538e3575014ab: function AS_FlexContainer_a799c7b8915340ec8c7538e3575014ab(eventobject) {
        var self = this;
        self.view.flxAttachImage.isVisible = false;
    },
    /** onCapture defined for cameraIDAudio **/
    AS_Camera_i21ba47f047b4c51bc9e9dbe0f939f27: function AS_Camera_i21ba47f047b4c51bc9e9dbe0f939f27(eventobject) {
        var self = this;
        self.assignToImages.call(this, eventobject);
        self.view.flxImageUploadedAudio.isVisible = true;
    },
    /** onCapture defined for CameraVidCaptureAudio **/
    AS_Camera_a7c69ad2dd1e4ca2807eaf2fa023e6cd: function AS_Camera_a7c69ad2dd1e4ca2807eaf2fa023e6cd(eventobject) {
        var self = this;
        self.assignToVideos.call(this, eventobject);
        self.view.flxVidUploadedAudio.isVisible = true;
    },
    /** onClick defined for flxCameraVidAudio **/
    AS_FlexContainer_c5c5dd4bd8954a0ab30be9b622c436ff: function AS_FlexContainer_c5c5dd4bd8954a0ab30be9b622c436ff(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_b801adbe3dca446cad9d9afb42bcc4ab_True() {
            var ntf = new kony.mvc.Navigation("frmHomeContent");
            ntf.navigate();
        }
        function SHOW_ALERT_ide_onClick_b801adbe3dca446cad9d9afb42bcc4ab_False() {}
        function SHOW_ALERT_ide_onClick_b801adbe3dca446cad9d9afb42bcc4ab_Callback(response) {
            if (response === true) {
                SHOW_ALERT_ide_onClick_b801adbe3dca446cad9d9afb42bcc4ab_True();
            } else {
                SHOW_ALERT_ide_onClick_b801adbe3dca446cad9d9afb42bcc4ab_False();
            }
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "alertTitle": "Wonderful!!",
            "yesLabel": null,
            "noLabel": null,
            "alertIcon": null,
            "message": "Your file has been uploaded successfully",
            "alertHandler": SHOW_ALERT_ide_onClick_b801adbe3dca446cad9d9afb42bcc4ab_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_LEFT
        });
    },
    /** onClick defined for flxDeleteUploadAudio **/
    AS_FlexContainer_id742e81688d40838d997d52885185e2: function AS_FlexContainer_id742e81688d40838d997d52885185e2(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_hd843cf056654842809fdfaba6c27cfb_True() {
            self.onImageUploadedCancels.call(this);
            self.view.flxAttachImageForAudio.isVisible = false;
            self.view.flxImageUploadedAudio.isVisible = false;
        }
        function SHOW_ALERT_ide_onClick_hd843cf056654842809fdfaba6c27cfb_False() {}
        function SHOW_ALERT_ide_onClick_hd843cf056654842809fdfaba6c27cfb_Callback(response) {
            if (response === true) {
                SHOW_ALERT_ide_onClick_hd843cf056654842809fdfaba6c27cfb_True();
            } else {
                SHOW_ALERT_ide_onClick_hd843cf056654842809fdfaba6c27cfb_False();
            }
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_CONFIRMATION,
            "alertTitle": "Cancel Attachment",
            "yesLabel": "YES",
            "noLabel": "NO",
            "alertIcon": null,
            "message": "Your image will be deleted. Would you like to proceed?",
            "alertHandler": SHOW_ALERT_ide_onClick_hd843cf056654842809fdfaba6c27cfb_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
        });
    },
    /** onCapture defined for cmrRetakeAudio **/
    AS_Camera_eefbfb1edc44484fa59a426d0bb0b0e6: function AS_Camera_eefbfb1edc44484fa59a426d0bb0b0e6(eventobject) {
        var self = this;
        return self.assignToImage.call(this, eventobject);
    },
    /** onClick defined for flxRetakeAudio **/
    AS_FlexContainer_c713803333494edb907540e15fbd9f45: function AS_FlexContainer_c713803333494edb907540e15fbd9f45(eventobject) {
        var self = this;

        function ___ide_onClick_hfe739031d124a9e90e76ee8a5f46420_Callback() {}
        self.view.flxAttachFile.animate(
        kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "top": "0%"
            }
        }), {
            "delay": 0,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_hfe739031d124a9e90e76ee8a5f46420_Callback
        });
    },
    /** onClick defined for flxUseImageAudio **/
    AS_FlexContainer_c41b45eb2898478c8bca19a6969f4f39: function AS_FlexContainer_c41b45eb2898478c8bca19a6969f4f39(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_e8ca897a39a04c24ad9aee344cef970a_True() {
            self.view.flxAttachImageForAudio.isVisible = false;
            self.view.flxImageUploadedAudio.isVisible = false;
            self.view.imgImageConfiemation.isVisible = true;
            self.view.lblAttachConfirmation.right = "20%";
            self.view.flxDeleteAttached.isVisible = true;
        }
        function SHOW_ALERT_ide_onClick_e8ca897a39a04c24ad9aee344cef970a_Callback() {
            SHOW_ALERT_ide_onClick_e8ca897a39a04c24ad9aee344cef970a_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_INFO,
            "alertTitle": "Image Attached!!",
            "yesLabel": "Ok...Got it!",
            "message": "Your image file has been successfully attached.",
            "alertHandler": SHOW_ALERT_ide_onClick_e8ca897a39a04c24ad9aee344cef970a_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
        });
        self.onImageFileUploads.call(this, eventobject);
        var ntf = new kony.mvc.Navigation("frmHomeContent");
        ntf.navigate({
            "imgCaptured_isVisible": self.view.imgCaptured.isVisible,
            "imgCaptured_base64": self.view.imgCaptured.base64,
            "imgCaptured_src": self.view.imgCaptured.src,
            "_meta_": {
                "eventName": "onClick",
                "widgetId": "flxUseImageAudio"
            }
        });
    },
    /** onClick defined for flxDeleteVidAudio **/
    AS_FlexContainer_i734da44b1064596a0e147652a7c85cc: function AS_FlexContainer_i734da44b1064596a0e147652a7c85cc(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("frmHomeContent");
        ntf.navigate();
        self.view.imgCapturedAudio.isVisible = false;
        self.view.flxVidUploadedAudio.isVisible = false;
        self.view.flxAttachImageForAudio.isVisible = false;
    },
    /** onCapture defined for cmrVidRetakeAudio **/
    AS_Camera_cd122a427d134adeac21e139b3fc38f2: function AS_Camera_cd122a427d134adeac21e139b3fc38f2(eventobject) {
        var self = this;
        return self.assignToVideo.call(this, eventobject);
    },
    /** onClick defined for flxRetakeVidAudio **/
    AS_FlexContainer_f053bf089be948e0b4b6fdc2996f32ff: function AS_FlexContainer_f053bf089be948e0b4b6fdc2996f32ff(eventobject) {
        var self = this;

        function ___ide_onClick_j65ea55c62b846f68a6bc99d6c609e34_Callback() {}
        self.view.flxAttachFile.animate(
        kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "top": "0%"
            }
        }), {
            "delay": 0,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_j65ea55c62b846f68a6bc99d6c609e34_Callback
        });
    },
    /** onClick defined for flxUseVidAudio **/
    AS_FlexContainer_e8e525b10a084ca68989e8b9bb2521d8: function AS_FlexContainer_e8e525b10a084ca68989e8b9bb2521d8(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_be691a9dda04430b8a4defd78a59ae9c_True() {
            self.view.flxAttachImageForAudio.isVisible = false;
            self.view.imgVidConfirmation.isVisible = true;
            self.view.imgImageConfiemation.isVisible = false;
            self.view.flxDeleteAttached.isVisible = true;
            self.view.lblAttachConfirmation.right = "18%";
            self.view.flxDeleteAttached.isVisible = true;
        }
        function SHOW_ALERT_ide_onClick_be691a9dda04430b8a4defd78a59ae9c_Callback() {
            SHOW_ALERT_ide_onClick_be691a9dda04430b8a4defd78a59ae9c_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_INFO,
            "alertTitle": "Video Attached!!",
            "yesLabel": "OK...Got it!",
            "message": "Your video has been successfully attached.",
            "alertHandler": SHOW_ALERT_ide_onClick_be691a9dda04430b8a4defd78a59ae9c_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
        });
        self.onVideoFileUploads.call(this, eventobject);
        var ntf = new kony.mvc.Navigation("frmHomeContent");
        ntf.navigate();
    },
    /** onClick defined for flxAttachImageForAudio **/
    AS_FlexContainer_b32cc503b3a6479c9966c58e6d065aa7: function AS_FlexContainer_b32cc503b3a6479c9966c58e6d065aa7(eventobject) {
        var self = this;
        self.view.flxAttachImageForAudio.isVisible = false;
    },
    /** onClick defined for invisibleButton **/
    AS_Button_dcdc41e3511f4b14bf1468020f6713ee: function AS_Button_dcdc41e3511f4b14bf1468020f6713ee(eventobject) {
        var self = this;

        function ___ide_onClick_f252f920ff0748f4bc2571f74d8ee7d9_Callback() {
            self.view.invisibleButton.isVisible = false;
            self.view.visibleButton.isVisible = true;
        }
        undefined.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "duration": 0.25,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_f252f920ff0748f4bc2571f74d8ee7d9_Callback
        });
    },
    /** onClick defined for navigationBackButton **/
    AS_Button_da252cfadf1145a28997a7046f19ed74: function AS_Button_da252cfadf1145a28997a7046f19ed74(eventobject) {
        var self = this;
        var frmSearch = new kony.mvc.Navigation("frmSearch");
        frmSearch.navigate();
    }
});