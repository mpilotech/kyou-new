define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for flxHamburger **/
    AS_FlexContainer_ddd434ba2a704499923b150b7f028afe: function AS_FlexContainer_ddd434ba2a704499923b150b7f028afe(eventobject, x, y) {
        var self = this;

        function ___ide_onTouchStart_g09aa3df91a24711960e4d3dfd2ddb4a_Callback() {}
        function ___ide_onTouchStart_e4f471485f9e403da1d9d4eaaf7c66bb_Callback() {}
        self.view.Hamburger.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": ___ide_onTouchStart_e4f471485f9e403da1d9d4eaaf7c66bb_Callback
        });
        self.view.flxContentMain.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "80%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": ___ide_onTouchStart_g09aa3df91a24711960e4d3dfd2ddb4a_Callback
        });
    },
    /** onClick defined for flxTextTab **/
    AS_FlexContainer_f96e71f1900349a299406e37a946b34e: function AS_FlexContainer_f96e71f1900349a299406e37a946b34e(eventobject) {
        var self = this;

        function ___ide_onClick_a1bf40a39d004ff187865c6429f3624e_Callback() {}
        self.view.flxColour1.isVisible = true;
        self.view.flxColour2.isVisible = false;
        self.view.flxColour3.opacity = "0";
        self.view.flxTextAudioSurveyContent.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "direction": kony.anim.DIRECTION_ALTERNATE,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_a1bf40a39d004ff187865c6429f3624e_Callback
        });
    },
    /** onClick defined for flxAudioTab **/
    AS_FlexContainer_hb5389f28f54490083a86d6049f707f9: function AS_FlexContainer_hb5389f28f54490083a86d6049f707f9(eventobject) {
        var self = this;

        function ___ide_onClick_d3d0c7bad2d4446f904ab5d5ee5fe29a_Callback() {}
        self.view.flxColour1.isVisible = false;
        self.view.flxColour2.isVisible = true;
        self.view.flxColour3.opacity = "0";
        self.view.flxTextAudioSurveyContent.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "direction": kony.anim.DIRECTION_ALTERNATE,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_d3d0c7bad2d4446f904ab5d5ee5fe29a_Callback
        });
    },
    /** onClick defined for flxSurveyTab **/
    AS_FlexContainer_b21bfdfb8d7444e8b568d42ed539c1dd: function AS_FlexContainer_b21bfdfb8d7444e8b568d42ed539c1dd(eventobject) {
        var self = this;
    },
    /** onClick defined for flxDeleteAttachment **/
    AS_FlexContainer_e95a19f330ba4cc5b125cb6f579a344a: function AS_FlexContainer_e95a19f330ba4cc5b125cb6f579a344a(eventobject) {
        var self = this;
    },
    /** onClick defined for flxAttachFile **/
    AS_FlexContainer_f5b34b71416a460b9ddc010bd30f0ce5: function AS_FlexContainer_f5b34b71416a460b9ddc010bd30f0ce5(eventobject) {
        var self = this;

        function ___ide_onClick_fd16397026264f448ead3e5193f35387_Callback() {}
        self.view.flxAttachImage.isVisible = true;
        self.view.flxAttachImage.animate(
        kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "top": "0%"
            }
        }), {
            "delay": 0,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_fd16397026264f448ead3e5193f35387_Callback
        });
    },
    /** onClick defined for flxDelete **/
    AS_FlexContainer_h2a6d022426d4709b530b6a83e430bfb: function AS_FlexContainer_h2a6d022426d4709b530b6a83e430bfb(eventobject) {
        var self = this;
    },
    /** onClick defined for flxSendText **/
    AS_FlexContainer_a4d069b1eb53405582cad1c797e97184: function AS_FlexContainer_a4d069b1eb53405582cad1c797e97184(eventobject) {
        var self = this;

        function SHOW_ALERT_ide_onClick_a9174231e6b74504ae8983ee0799f5f6_True() {}
        function SHOW_ALERT_ide_onClick_a9174231e6b74504ae8983ee0799f5f6_Callback() {
            SHOW_ALERT_ide_onClick_a9174231e6b74504ae8983ee0799f5f6_True();
        }
        kony.ui.Alert({
            "alertType": constants.ALERT_TYPE_INFO,
            "alertTitle": "Message Sent!!",
            "yesLabel": "OK",
            "message": "Your text message has been received. We will attempt to resolve it ASAP. Thank you.",
            "alertHandler": SHOW_ALERT_ide_onClick_a9174231e6b74504ae8983ee0799f5f6_Callback
        }, {
            "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
        });
    },
    /** recordSuccessCallback defined for audio **/
    AS_UWI_c91a216a1af5453e97c5fe12d2bc7b47: function AS_UWI_c91a216a1af5453e97c5fe12d2bc7b47() {
        var self = this;
    },
    /** recordErrorCallback defined for audio **/
    AS_UWI_c9836e44be38462cb934c8abdd09fbc7: function AS_UWI_c9836e44be38462cb934c8abdd09fbc7(error) {
        var self = this;
    },
    /** onClick defined for flxRecordIcon **/
    AS_FlexContainer_a5ca18deec944e54a60296bbf059ae71: function AS_FlexContainer_a5ca18deec944e54a60296bbf059ae71(eventobject) {
        var self = this;
    },
    /** onClick defined for flxReplayContent **/
    AS_FlexContainer_f517a72473b64660b4ca4703b1f4efba: function AS_FlexContainer_f517a72473b64660b4ca4703b1f4efba(eventobject) {
        var self = this;
    },
    /** onClick defined for flxCancelContent **/
    AS_FlexContainer_ceb2e68d726545368513efd4bcb96ce1: function AS_FlexContainer_ceb2e68d726545368513efd4bcb96ce1(eventobject) {
        var self = this;
    },
    /** onClick defined for flxAttachContent **/
    AS_FlexContainer_b586b9eb562e4fffbdb4ef284a87482d: function AS_FlexContainer_b586b9eb562e4fffbdb4ef284a87482d(eventobject) {
        var self = this;

        function ___ide_onClick_ef6d92bf14bd444184999ba16b19de27_Callback() {}
        self.view.flxAttachImageForAudio.isVisible = true;
        self.view.flxAttachImageForAudio.animate(
        kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "top": "0%"
            }
        }), {
            "delay": 0,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_ef6d92bf14bd444184999ba16b19de27_Callback
        });
    },
    /** onClick defined for flxSendContent **/
    AS_FlexContainer_db90026b43ed4d198c9db6a01cae43a5: function AS_FlexContainer_db90026b43ed4d198c9db6a01cae43a5(eventobject) {
        var self = this;
    },
    /** onClick defined for flxDeleteAttached **/
    AS_FlexContainer_f54ada8888804400aa9d9616848d8da5: function AS_FlexContainer_f54ada8888804400aa9d9616848d8da5(eventobject) {
        var self = this;
    },
    /** onCapture defined for cameraID **/
    AS_Camera_e9afe3c80d564b29a8ad6e54bccaca6a: function AS_Camera_e9afe3c80d564b29a8ad6e54bccaca6a(eventobject) {
        var self = this;
    },
    /** onCapture defined for CameraVidCapture **/
    AS_Camera_cd180eba4a7946efb064e1d081d05b0f: function AS_Camera_cd180eba4a7946efb064e1d081d05b0f(eventobject) {
        var self = this;
    },
    /** onClick defined for flxCameraVid **/
    AS_FlexContainer_eef7cf88251e43f18509292ccbc460a4: function AS_FlexContainer_eef7cf88251e43f18509292ccbc460a4(eventobject) {
        var self = this;
    },
    /** onClick defined for flxDeleteUpload **/
    AS_FlexContainer_d864a2019a194d8c9163623af6cfc089: function AS_FlexContainer_d864a2019a194d8c9163623af6cfc089(eventobject) {
        var self = this;
    },
    /** onCapture defined for cmrRetake **/
    AS_Camera_cca20402a52f42e282d2b71fd1f4bb4a: function AS_Camera_cca20402a52f42e282d2b71fd1f4bb4a(eventobject) {
        var self = this;
    },
    /** onClick defined for flxRetake **/
    AS_FlexContainer_d87dfaa791fc49b5a644b42806cccc16: function AS_FlexContainer_d87dfaa791fc49b5a644b42806cccc16(eventobject) {
        var self = this;

        function ___ide_onClick_dfba198fd32c41d9a3efd3025af015c0_Callback() {}
        self.view.flxAttachFile.animate(
        kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "top": "0%"
            }
        }), {
            "delay": 0,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_dfba198fd32c41d9a3efd3025af015c0_Callback
        });
    },
    /** onClick defined for flxUseImage **/
    AS_FlexContainer_cac7804e0d1047fa9e59dfda97b689d3: function AS_FlexContainer_cac7804e0d1047fa9e59dfda97b689d3(eventobject) {
        var self = this;
    },
    /** onClick defined for flxDeleteVid **/
    AS_FlexContainer_dc20bb16ce2b4af490b42c92cfab51f6: function AS_FlexContainer_dc20bb16ce2b4af490b42c92cfab51f6(eventobject) {
        var self = this;
    },
    /** onCapture defined for cmrVidRetake **/
    AS_Camera_a6d99d4184f5466c999f5b912f07c101: function AS_Camera_a6d99d4184f5466c999f5b912f07c101(eventobject) {
        var self = this;
    },
    /** onClick defined for flxRetakeVid **/
    AS_FlexContainer_aa58fb382dad4b08ae3abd6ce1226593: function AS_FlexContainer_aa58fb382dad4b08ae3abd6ce1226593(eventobject) {
        var self = this;

        function ___ide_onClick_bf0d22a8e8b347a1a4621517c41c4b75_Callback() {}
        self.view.flxAttachFile.animate(
        kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "top": "0%"
            }
        }), {
            "delay": 0,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_bf0d22a8e8b347a1a4621517c41c4b75_Callback
        });
    },
    /** onClick defined for flxUseVid **/
    AS_FlexContainer_ae4c34d8432e4f2ba7e8bb2af2fad5a0: function AS_FlexContainer_ae4c34d8432e4f2ba7e8bb2af2fad5a0(eventobject) {
        var self = this;
    },
    /** onClick defined for flxAttachImage **/
    AS_FlexContainer_a799c7b8915340ec8c7538e3575014ab: function AS_FlexContainer_a799c7b8915340ec8c7538e3575014ab(eventobject) {
        var self = this;
        self.view.flxAttachImage.isVisible = false;
    },
    /** onCapture defined for cameraIDAudio **/
    AS_Camera_i21ba47f047b4c51bc9e9dbe0f939f27: function AS_Camera_i21ba47f047b4c51bc9e9dbe0f939f27(eventobject) {
        var self = this;
    },
    /** onCapture defined for CameraVidCaptureAudio **/
    AS_Camera_a7c69ad2dd1e4ca2807eaf2fa023e6cd: function AS_Camera_a7c69ad2dd1e4ca2807eaf2fa023e6cd(eventobject) {
        var self = this;
    },
    /** onClick defined for flxCameraVidAudio **/
    AS_FlexContainer_c5c5dd4bd8954a0ab30be9b622c436ff: function AS_FlexContainer_c5c5dd4bd8954a0ab30be9b622c436ff(eventobject) {
        var self = this;
    },
    /** onClick defined for flxDeleteUploadAudio **/
    AS_FlexContainer_id742e81688d40838d997d52885185e2: function AS_FlexContainer_id742e81688d40838d997d52885185e2(eventobject) {
        var self = this;
    },
    /** onCapture defined for cmrRetakeAudio **/
    AS_Camera_eefbfb1edc44484fa59a426d0bb0b0e6: function AS_Camera_eefbfb1edc44484fa59a426d0bb0b0e6(eventobject) {
        var self = this;
    },
    /** onClick defined for flxRetakeAudio **/
    AS_FlexContainer_c713803333494edb907540e15fbd9f45: function AS_FlexContainer_c713803333494edb907540e15fbd9f45(eventobject) {
        var self = this;

        function ___ide_onClick_hfe739031d124a9e90e76ee8a5f46420_Callback() {}
        self.view.flxAttachFile.animate(
        kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "top": "0%"
            }
        }), {
            "delay": 0,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_hfe739031d124a9e90e76ee8a5f46420_Callback
        });
    },
    /** onClick defined for flxUseImageAudio **/
    AS_FlexContainer_c41b45eb2898478c8bca19a6969f4f39: function AS_FlexContainer_c41b45eb2898478c8bca19a6969f4f39(eventobject) {
        var self = this;
    },
    /** onClick defined for flxDeleteVidAudio **/
    AS_FlexContainer_i734da44b1064596a0e147652a7c85cc: function AS_FlexContainer_i734da44b1064596a0e147652a7c85cc(eventobject) {
        var self = this;
    },
    /** onCapture defined for cmrVidRetakeAudio **/
    AS_Camera_cd122a427d134adeac21e139b3fc38f2: function AS_Camera_cd122a427d134adeac21e139b3fc38f2(eventobject) {
        var self = this;
    },
    /** onClick defined for flxRetakeVidAudio **/
    AS_FlexContainer_f053bf089be948e0b4b6fdc2996f32ff: function AS_FlexContainer_f053bf089be948e0b4b6fdc2996f32ff(eventobject) {
        var self = this;

        function ___ide_onClick_j65ea55c62b846f68a6bc99d6c609e34_Callback() {}
        self.view.flxAttachFile.animate(
        kony.ui.createAnimation({
            "100": {
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                },
                "top": "0%"
            }
        }), {
            "delay": 0,
            "duration": 0.4,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "iterationCount": 1
        }, {
            "animationEnd": ___ide_onClick_j65ea55c62b846f68a6bc99d6c609e34_Callback
        });
    },
    /** onClick defined for flxUseVidAudio **/
    AS_FlexContainer_e8e525b10a084ca68989e8b9bb2521d8: function AS_FlexContainer_e8e525b10a084ca68989e8b9bb2521d8(eventobject) {
        var self = this;
    },
    /** onClick defined for flxAttachImageForAudio **/
    AS_FlexContainer_b32cc503b3a6479c9966c58e6d065aa7: function AS_FlexContainer_b32cc503b3a6479c9966c58e6d065aa7(eventobject) {
        var self = this;
        self.view.flxAttachImageForAudio.isVisible = false;
    },
    /** onTouchStart defined for flxOverlay **/
    AS_FlexContainer_f698552fbf6a42b4a88d7d3e313c4bdf: function AS_FlexContainer_f698552fbf6a42b4a88d7d3e313c4bdf(eventobject, x, y) {
        var self = this;

        function MOVE_ACTION____e2d5b1f250914e99b4033569f3b7c65d_Callback() {}
        function MOVE_ACTION____cd9bcb830e124247ab73ace0a750d559_Callback() {}
        self.view.Hamburger.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "-100%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": MOVE_ACTION____cd9bcb830e124247ab73ace0a750d559_Callback
        });
        self.view.flxContentMain.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": MOVE_ACTION____e2d5b1f250914e99b4033569f3b7c65d_Callback
        });
    },
    /** onNavigate defined for frmLogCommplain **/
    onNavigate: function AS_Form_c3825ff059ed45718e24748b34297b1c(eventobject) {
        var self = this;
        return self.onDataPass.call(this, eventobject);
    }
});