define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnSignUp **/
    AS_Button_id4a9fb74c6642abbe56c62e0db18288: function AS_Button_id4a9fb74c6642abbe56c62e0db18288(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("AccountContent/frmSignUp");
        ntf.navigate();
    },
    /** onClick defined for btnSignIn **/
    AS_Button_f1db0ba17c444833a834fd805c3fce8c: function AS_Button_f1db0ba17c444833a834fd805c3fce8c(eventobject) {
        var self = this;
        var ntf = new kony.mvc.Navigation("AccountContent/frmSignIn");
        ntf.navigate();
    }
});