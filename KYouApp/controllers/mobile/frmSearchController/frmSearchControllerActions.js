define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchStart defined for flxHamburger **/
    AS_FlexContainer_cf410d81d2b746f79996bb3b52aefb94: function AS_FlexContainer_cf410d81d2b746f79996bb3b52aefb94(eventobject, x, y) {
        var self = this;

        function ___ide_onTouchStart_dba855eddb5c4fee8e2b8dc1a89ca1c2_Callback() {}
        function ___ide_onTouchStart_e0a1daf02b5149a892afc7803d65d96c_Callback() {}
        self.view.Hamburger.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "0%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": ___ide_onTouchStart_e0a1daf02b5149a892afc7803d65d96c_Callback
        });
        self.view.flxContent.animate(
        kony.ui.createAnimation({
            "100": {
                "left": "80%",
                "stepConfig": {
                    "timingFunction": kony.anim.EASE
                }
            }
        }), {
            "delay": 0,
            "iterationCount": 1,
            "fillMode": kony.anim.FILL_MODE_FORWARDS,
            "duration": 0.4
        }, {
            "animationEnd": ___ide_onTouchStart_dba855eddb5c4fee8e2b8dc1a89ca1c2_Callback
        });
    },
    /** onPinClick defined for placesMap **/
    AS_Map_c5289b1e752f4bddb61db871922bb9a9: function AS_Map_c5289b1e752f4bddb61db871922bb9a9(eventobject, location) {
        var self = this;
        return self.pinClicked.call(this, eventobject);
    },
    /** onSelection defined for placesMap **/
    AS_Map_bc022558a2cd4db7a32b202b5d9f4690: function AS_Map_bc022558a2cd4db7a32b202b5d9f4690(eventobject, location) {
        var self = this;
        return self.calloutClicked.call(this, eventobject, location);
    },
    /** onMapLoaded defined for placesMap **/
    AS_Map_b488b7061117472aa85d3c4d07f60ff9: function AS_Map_b488b7061117472aa85d3c4d07f60ff9(eventobject) {
        var self = this;
        return self.mapData.call(this, eventobject);
    }
});