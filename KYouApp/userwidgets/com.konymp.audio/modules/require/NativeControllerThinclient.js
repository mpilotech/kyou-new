define(['./Inherits', './NativeController', './KonyLogger'], function(Inherits, NativeController, konyLoggerModule) {
  var konymp = konymp || {};
  konymp.logger = new konyLoggerModule("NativeControllerThinclient");
  var NativeControllerThinclient = function(componentInstance) {
    konymp.logger.trace("-- Start constructor NativeControllerThinclient --", konymp.logger.FUNCTION_ENTRY);
     NativeController.call(this,componentInstance); 
    this.componentInstance = componentInstance;
    this.audioChunks = [];
    this.mediaRecorder = null;
    this.Recorder = require("com/konymp/audio/Recorder");
    konymp.logger.trace("-- Exit constructor NativeControllerThinclient -- ", konymp.logger.FUNCTION_EXIT);
  };
  Inherits(NativeControllerThinclient, NativeController);
  /**
   * @function
   *
   */
  NativeControllerThinclient.prototype._configRecorder = function(){

  };
  /**
  	 * @function
  	 *
  	 */
  NativeControllerThinclient.prototype.startRecord = function(){
    try{
      navigator.mediaDevices.getUserMedia({ audio: true })
        .then(stream => {
        if(this.mediaRecorder===null)
        {var AudioContext = window.AudioContext || window.webkitAudioContext;
         var audioContext = new AudioContext();
         var input = audioContext.createMediaStreamSource(stream);
         this.mediaRecorder = new this.Recorder(input,{numChannels:1});
         this.mediaRecorder.record();  
        }
        this.mediaRecorder.record();
      });
    }
    catch(e){
      throw e;
    }
  };
  /**
     * @function
     *
     * @param blob 
     */
  NativeControllerThinclient.prototype._getWAV = function(blob){
    this.audioBlob = blob;
    const audioUrl = URL.createObjectURL(this.audioBlob);
    this.audio = new Audio(audioUrl);
    if(this.componentInstance.recordSuccessCallback!==undefined)
    {
      this.componentInstance.recordSuccessCallback(blob);
    }
  };
  /**
   * @function
   *
   */
  NativeControllerThinclient.prototype.stopRecording = function(){
    this.mediaRecorder.stop();
    this.mediaRecorder.exportWAV(this._getWAV.bind(this));
  };
  /**
   * @function
   *
   */
  NativeControllerThinclient.prototype.playRecorded = function(){
    if(this.audio.paused){
      this.audio.play();
    }
    else{
      this.audio.pause();
    }
  };
  /**
   * @function
   *
   */
  NativeControllerThinclient.prototype.isrecordedFileExist = function(){
    return this.mediaRecorder!==null>0?true:false;
  };
  /**
   * @function
   *
   */
  NativeControllerThinclient.prototype.deleteRecordFile = function(){
    if(this.mediaRecorder!==null){
      this.mediaRecorder.clear();
    }
  };
  /**
   * @function
   *
   */
  NativeControllerThinclient.prototype.shareRecordFile = function(){
    this.mediaRecorder.exportWAV(function(blob){
      var url = URL.createObjectURL(blob);
      var link = document.createElement('a');
      link.href = url;
      link.download = new Date().toISOString() + '.wav';
      link.innerHTML = link.download;
      link.click();
    });   
  };
  return NativeControllerThinclient;
});