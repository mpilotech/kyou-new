define([],function () {
  var NativeController=function(componentInstance){
    this.componentInstance=componentInstance;
    this.fileObj=null;
    this.recordObject=null;
 
  };
  /**
   * @function
   *
   */
  NativeController.prototype._configRecorder = function(){
    //createFile
    var mainLoc = kony.io.FileSystem.getDataDirectoryPath();
    var myFileLoc = "";
    myFileLoc = mainLoc + constants.FILE_PATH_SEPARATOR +"konymp_audio_Recorder.aiff";
    //#ifdef android
    myFileLoc = mainLoc + constants.FILE_PATH_SEPARATOR +"konymp_audio_Recorder.m4a";
    //#endif
    this.fileObj =  new kony.io.File(myFileLoc);
    this.fileLocation = myFileLoc;
  };

  /**
   * @function
   *
   */
  NativeController.prototype.startRecord = function(){
    try{
       
      if(!this.fileObj.exists())
      {
        this.fileObj.createFile();
      }
      var config = {
        onSuccess:this._recordSuccessCallback.bind(this,this.componentInstance.recordSuccessCallback),
        onFailure:this._recordErrorCallback.bind(this,this.componentInstance.recordErrorCallback)
      };
      this.recordObject = kony.media.record(this.fileObj,config);
      this.recordObject.startRecording();
    }
    catch(error){
      throw error;
    }
  };
  /**
   * @function
   *
   */
  NativeController.prototype._recordSuccessCallback = function(recordSuccessCallback){
      //below mentioned callbacks are for refference , need to be implemented for future..
      /**
       * @Author : sumeet.bartha@kony.com 
       * @date : 7, Jan 2019, 2:14 PM 
       * @team : marketPlace team 
      */
      function OnMediaProgress(Position) 
      {

      }

      function OnMediaCompleted() 
      {
      } 

      function OnMediaFailed(errorMessage) 
      {

      }
      var file = new kony.io.File(this.fileLocation);
      if(this.mediaObj!==undefined)
      {
        this.mediaObj.stop(); 
        this.mediaObj.releaseMedia();
      }
      this.mediaObj =  kony.media.createFromFile(file);
      this.mediaObj.setCallbacks({ onProgressCallBack: OnMediaProgress, onMediaCompleted: OnMediaCompleted.bind(this), onMediaFailed: OnMediaFailed });
     if(typeof(recordSuccessCallback)==='function'){  
    recordSuccessCallback();
   	 }
  };
  /**
   * @function
   *
   */
  NativeController.prototype._recordErrorCallback = function(recordErrorCallback, errorMessage){
    if(typeof(recordErrorCallback)==='function'){
      recordErrorCallback(errorMessage);
    }
  };
  /**
    * @function
    *
    */
  NativeController.prototype.stopRecording = function(){
    if(this.recordObject !== null){
      this.recordObject.stopRecording();
    }
  };
  /**
   * @function
   *
   */
  NativeController.prototype.playRecorded = function(){
    if(this.fileObj!==undefined && this.fileObj!==null && this.mediaObj!==undefined)
    {
      if(!this.mediaObj.isPlaying)
      {
        this.mediaObj.play(1);
      }else{
        this.mediaObj.pause();	
      }
    }
  };
  /**
   * @function
   *
   */
  NativeController.prototype.isrecordedFileExist = function(){
    if(this.fileObj!==undefined && this.fileObj!==null){
      return this.fileObj.exists();
    }else{
      return false;
    }
  };
  

  /**
   * @function
   *
   */
  NativeController.prototype.shareRecordFile = function(){
    if(this.fileLocation!==undefined && this.fileLocation!==null){
      this.componentInstance.view.socialshare.shareWithFilepath([this.fileLocation]);
    }
  };
  /**
   * @function
   *
   */
  NativeController.prototype.startAnimation = function(){

    this.componentInstance.view.imgRecordIcon.animate(
      kony.ui.createAnimation({
        "100": {
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          },
          opacity:1,
        },
        "50":{
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          },
          opacity:0
        },
        "0":{
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          },
          opacity:1
        }
      }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.75
      }, {
        /**
         * @function
         *
         */
        "animationEnd": function(){
          if(this.componentInstance._isRecording){
            this.startAnimation();
          }
        }.bind(this)
      });
  };
  /**
   * @function
   *
   */
  NativeController.prototype.stopAnimation = function(){
	 this.componentInstance.view.imgRecordIcon.opacity = 1;
  };
  return NativeController;
});