define(['./Inherits', './NativeController', './KonyLogger'], function(Inherits, NativeController, konyLoggerModule) {
    var konymp = konymp || {};
    konymp.logger = new konyLoggerModule("NativeControllerAndroid");
    var NativeControllerAndroid = function(componentInstance) {
        konymp.logger.trace("-- Start constructor NativeControllerAndroid --", konymp.logger.FUNCTION_ENTRY);
         NativeController.call(this,componentInstance); 
        konymp.logger.trace("-- Exit constructor NativeControllerAndroid -- ", konymp.logger.FUNCTION_EXIT);
    };
    Inherits(NativeControllerAndroid, NativeController);
  
    return NativeControllerAndroid;
});