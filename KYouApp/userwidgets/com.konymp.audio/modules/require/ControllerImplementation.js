define([], function() {
  var ControllerImplementation = function(componentInstance, componentName) {
    this.componentInstance = componentInstance;
    this.componentName="audio";
    this.getNativeController = function() {
      try {
        if (this.nativeControllerInstance === undefined) {
        var deviceInfo=kony.os.deviceInfo();
        var platformName=null;
        if(deviceInfo.name.toLowerCase()==='iphone')
        {
          platformName='IOS.js';
        }else if(deviceInfo.name.toLowerCase()==='android'){
          platformName='Android.js';
        }else{
          platformName=deviceInfo.name.charAt(0).toUpperCase()+deviceInfo.name.slice(1);
        }
        var nativeControllerPath='com/konymp/audio'+'/NativeController'+platformName;
        var nativeController=require(nativeControllerPath);
        this.nativeControllerInstance=new nativeController(componentInstance);
      }
      return this.nativeControllerInstance;
    
      } catch (exception) {
        throw new Error(exception);
      }

    };
  };
  /**
   * @function
   *
   */
  ControllerImplementation.prototype._configRecorder = function(){
    this.getNativeController()._configRecorder();
  };
  /**
   * @function
   *
   */
  ControllerImplementation.prototype._recordSuccessCallback = function(){
    this.getNativeController()._recordSuccessCallback();
  };
  /**
   * @function
   *
   */
  ControllerImplementation.prototype._recordErrorCallback = function(){
    this.getNativeController()._recordErrorCallback();
  };
  /**
   * @function
   *
   */
  ControllerImplementation.prototype.startRecord = function(){
    this.getNativeController().startRecord();
  };
  /**
    * @function
    *
    */
  ControllerImplementation.prototype.stopRecording = function(){ 
    this.getNativeController().stopRecording();
  };
  /**
   * @function
   *
   */
  ControllerImplementation.prototype.playRecorded = function(){
    this.getNativeController().playRecorded();
  };
  /**
   * @function
   *
   */
  ControllerImplementation.prototype.isrecordedFileExist = function(){
    return this.getNativeController().isrecordedFileExist();
  };

   /**
    * @function
    *
    */
   ControllerImplementation.prototype.shareRecordFile = function(){  
    this.getNativeController().shareRecordFile();
  };
  /**
   * @function
   *
   */
  ControllerImplementation.prototype.startAnimation = function(){
     this.getNativeController().startAnimation();
  };
  /**
   * @function
   *
   */
  ControllerImplementation.prototype.stopAnimation = function(){
      this.getNativeController().stopAnimation();
  };
  return ControllerImplementation;
});