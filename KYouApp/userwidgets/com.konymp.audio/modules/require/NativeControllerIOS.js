define(['./Inherits', './NativeController', './KonyLogger'], function(Inherits, NativeController, konyLoggerModule) {
    var konymp = konymp || {};
    konymp.logger = new konyLoggerModule("NativeControllerIOS");
    var NativeControllerIOS = function(componentInstance) {
        konymp.logger.trace("-- Start constructor NativeControllerIOS --", konymp.logger.FUNCTION_ENTRY);
         NativeController.call(this,componentInstance); 
        konymp.logger.trace("-- Exit constructor NativeControllerIOS -- ", konymp.logger.FUNCTION_EXIT);
    };
    Inherits(NativeControllerIOS, NativeController);
  
    return NativeControllerIOS;
});