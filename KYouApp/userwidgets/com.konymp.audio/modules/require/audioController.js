define(['./ControllerImplementation', './KonyLogger'],function(ControllerImplementation,KonyLogger) {
  var konymp = konymp || {};
  konymp.logger = new KonyLogger("audio/audioController");
  return {
    /**
     * @constructor
     *
     * @param baseConfig 
     * @param layoutConfig 
     * @param pspConfig 
     */
    constructor: function(baseConfig, layoutConfig, pspConfig) {
      konymp.logger.trace("----------------------------- constructor ENTRY", konymp.logger.FUNCTION_ENTRY);
      this.handler = new ControllerImplementation(this, baseConfig.id);
      this.isConfigured = 0;
      this._isRecording = false;
      this._configRecorder();
      konymp.logger.trace("----------------------------- constructor EXIT", konymp.logger.FUNCTION_EXIT);
    },
    //Logic for getters/setters of custom properties
    initGettersSetters: function() {
	/**
       * @function
       * @property : iconImage
       * @description : Image for the icon to be shown on the screen of audio widget.
       * @return : string
       */
     
    },
     /**
     * @event : recordSuccessCallback
     * @description : This event is invoked audio is successfully recorded. 
     */
    recordSuccessCallback:function(){},
     /**
     * @event : recordErrorCallback
     * @description : This event is invoked  if there is any error while recording audio.
     */
    recordErrorCallback:function(){},
    /**
     * @function
     *
     */
    _configRecorder:function(){
      konymp.logger.trace("----------------------------- configRecorder ENTRY", konymp.logger.FUNCTION_ENTRY);
      this.handler._configRecorder();
      this.isConfigured = 1;
      konymp.logger.trace("----------------------------- configRecorder EXIT", konymp.logger.FUNCTION_EXIT); 
    },
    /**
     * @api : startRecord
     * @description : This function starts the recording of audio.
     * @return : null
     */
    startRecord:function(){
      konymp.logger.trace("----------------------------- startRecord ENTRY", konymp.logger.FUNCTION_ENTRY);
      if(!this._isRecording){
        if(this.isConfigured !== 1){
          this._configRecorder();
        }
        this._isRecording = true;
        this.startAnimation();
        this.handler.startRecord();
      }
      konymp.logger.trace("----------------------------- startRecord EXIT", konymp.logger.FUNCTION_EXIT); 
    },
     /**
     * @api : stopRecord
     * @description : This function stops the recording of audio.
     * @return : null
     */
    stopRecord:function(){
      konymp.logger.trace("----------------------------- stopRecord ENTRY", konymp.logger.FUNCTION_ENTRY);
      if(this._isRecording){
        this.handler.stopRecording();
        this._isRecording=false;
        this.stopAnimation();
      }
      konymp.logger.trace("----------------------------- stopRecord EXIT", konymp.logger.FUNCTION_EXIT); 

    },
      /**
     * @api : TogglePlayPause
     * @description : This function toggles the playing of audio.
     * @remarks : Audio can't be played if recording or no recording is done.
     * @return : null
     */
    togglePlayPause:function(){
      konymp.logger.trace("----------------------------- toggleRecordPlay ENTRY", konymp.logger.FUNCTION_ENTRY);
      if(!this._isRecording){
        this.handler.playRecorded();
      }
      konymp.logger.trace("----------------------------- toggleRecordPlay EXIT", konymp.logger.FUNCTION_EXIT); 
    },
    /**
     * @api : isrecordedFileExist
     * @description : This function returns true if record file exist.
     * @remarks : Available for Only Android and iOS platform.
     * @return : boolean
     */
    isrecordedFileExist:function(){
      konymp.logger.trace("----------------------------- isrecordedFileExist ENTRY", konymp.logger.FUNCTION_ENTRY);
      konymp.logger.trace("----------------------------- isrecordedFileExist EXIT", konymp.logger.FUNCTION_EXIT); 
      return this.handler.isrecordedFileExist();
    },
     /**
     * @api : shareRecordFile
     * @description : This function share the recorded file.
     * @remarks : For desktop web it save the file to desktop disk.
     * @return : null
     */
    shareRecordFile:function(){
      konymp.logger.trace("----------------------------- shareRecordFile ENTRY", konymp.logger.FUNCTION_ENTRY);
      this.handler.shareRecordFile();
      konymp.logger.trace("----------------------------- shareRecordFile EXIT", konymp.logger.FUNCTION_EXIT); 
    },
    /**
     * @function
     *
     */
    _toggleRecord:function(){
      if(!this._isRecording)
      {
        this.startRecord();
      }
      else{
        this.stopRecord();
      }
    },
    /**
     * @function
     *
     */
    startAnimation:function(){
      konymp.logger.trace("----------------------------- startAnimation ENTRY", konymp.logger.FUNCTION_ENTRY);
      this.handler.startAnimation();
      konymp.logger.trace("----------------------------- startAnimation EXIT", konymp.logger.FUNCTION_EXIT); 

    },
    /**
     * @function
     *
     */
    stopAnimation:function(){
      konymp.logger.trace("----------------------------- stopAnimation ENTRY", konymp.logger.FUNCTION_ENTRY);
      this.handler.stopAnimation();
      konymp.logger.trace("----------------------------- stopAnimation EXIT", konymp.logger.FUNCTION_EXIT); 

    }
  };
});