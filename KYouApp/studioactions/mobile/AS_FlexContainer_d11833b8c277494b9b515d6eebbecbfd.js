function AS_FlexContainer_d11833b8c277494b9b515d6eebbecbfd(eventobject, x, y) {
    function MOVE_ACTION____c8bb5e127c91423cac1d9ea854dcdf1c_Callback() {}

    function MOVE_ACTION____gf475aaeccc645b1a365df9f7605817e_Callback() {}
    self.view.Hamburger.animate(kony.ui.createAnimation({
        "100": {
            "left": "0%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.4
    }, {
        "animationEnd": MOVE_ACTION____gf475aaeccc645b1a365df9f7605817e_Callback
    });
    self.view.flxContent.animate(kony.ui.createAnimation({
        "100": {
            "left": "80%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.4
    }, {
        "animationEnd": MOVE_ACTION____c8bb5e127c91423cac1d9ea854dcdf1c_Callback
    });
}