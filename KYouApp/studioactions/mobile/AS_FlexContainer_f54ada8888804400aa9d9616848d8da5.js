function AS_FlexContainer_f54ada8888804400aa9d9616848d8da5(eventobject) {
    function SHOW_ALERT_ide_onClick_i33e877d336149d28305ce628b6a08f9_True() {
        self.view.imgImageConfiemation.isVisible = false;
        self.view.imgVidConfirmation.isVisible = false;
        self.view.flxDeleteAttached.isVisible = false;
        self.view.lblAttachConfirmation.right = "35%";
    }

    function SHOW_ALERT_ide_onClick_i33e877d336149d28305ce628b6a08f9_False() {}

    function SHOW_ALERT_ide_onClick_i33e877d336149d28305ce628b6a08f9_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_i33e877d336149d28305ce628b6a08f9_True();
        } else {
            SHOW_ALERT_ide_onClick_i33e877d336149d28305ce628b6a08f9_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "DELETE ATTACHMENT",
        "yesLabel": "YES",
        "noLabel": "NO",
        "alertIcon": null,
        "message": "Your attachment will be removed. Do you want to continue?",
        "alertHandler": SHOW_ALERT_ide_onClick_i33e877d336149d28305ce628b6a08f9_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
    });
}