function AS_FlexContainer_a4d069b1eb53405582cad1c797e97184(eventobject) {
    function SHOW_ALERT_ide_onClick_a9174231e6b74504ae8983ee0799f5f6_True() {}

    function SHOW_ALERT_ide_onClick_a9174231e6b74504ae8983ee0799f5f6_Callback() {
        SHOW_ALERT_ide_onClick_a9174231e6b74504ae8983ee0799f5f6_True();
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_INFO,
        "alertTitle": "Message Sent!!",
        "yesLabel": "OK",
        "message": "Your text message has been received. We will attempt to resolve it ASAP. Thank you.",
        "alertHandler": SHOW_ALERT_ide_onClick_a9174231e6b74504ae8983ee0799f5f6_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
    });
}