function AS_FlexContainer_db90026b43ed4d198c9db6a01cae43a5(eventobject) {
    function SHOW_ALERT_ide_onClick_g22860f429c947c1a9fc7c8299f3967b_True() {}

    function SHOW_ALERT_ide_onClick_g22860f429c947c1a9fc7c8299f3967b_Callback() {
        SHOW_ALERT_ide_onClick_g22860f429c947c1a9fc7c8299f3967b_True();
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_INFO,
        "alertTitle": "Audio Sent!!",
        "yesLabel": "OK ... Got it",
        "message": "Your audio message has been received. We will attempt your query ASAP. Thank you.",
        "alertHandler": SHOW_ALERT_ide_onClick_g22860f429c947c1a9fc7c8299f3967b_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
    });
}