function AS_FlexContainer_ceb2e68d726545368513efd4bcb96ce1(eventobject) {
    function SHOW_ALERT_ide_onClick_dd949d1961064731aa559b9a736b9d82_True() {}

    function SHOW_ALERT_ide_onClick_dd949d1961064731aa559b9a736b9d82_Callback() {
        SHOW_ALERT_ide_onClick_dd949d1961064731aa559b9a736b9d82_True();
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_INFO,
        "alertTitle": "Audio Message Canceled!!",
        "yesLabel": "OK",
        "message": "Your audio has stopped. You can do another record again.",
        "alertHandler": SHOW_ALERT_ide_onClick_dd949d1961064731aa559b9a736b9d82_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
    });
}