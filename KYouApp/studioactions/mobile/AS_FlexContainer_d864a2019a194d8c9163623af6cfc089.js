function AS_FlexContainer_d864a2019a194d8c9163623af6cfc089(eventobject) {
    function SHOW_ALERT_ide_onClick_cbf1c9c577e848d0843179e2da8e4de6_True() {
        self.view.flxAttachImage.isVisible = false;
        self.view.flxImageUploaded.isVisible = false;
        undefined.show();
    }

    function SHOW_ALERT_ide_onClick_cbf1c9c577e848d0843179e2da8e4de6_False() {}

    function SHOW_ALERT_ide_onClick_cbf1c9c577e848d0843179e2da8e4de6_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_cbf1c9c577e848d0843179e2da8e4de6_True();
        } else {
            SHOW_ALERT_ide_onClick_cbf1c9c577e848d0843179e2da8e4de6_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "CANCEL ATTACHMENT",
        "yesLabel": "YES",
        "noLabel": "NO",
        "alertIcon": null,
        "message": "Your Image will be deleted. Would you like to proceed?",
        "alertHandler": SHOW_ALERT_ide_onClick_cbf1c9c577e848d0843179e2da8e4de6_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
    });
}