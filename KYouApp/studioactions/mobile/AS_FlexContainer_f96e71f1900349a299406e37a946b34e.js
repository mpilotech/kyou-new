function AS_FlexContainer_f96e71f1900349a299406e37a946b34e(eventobject) {
    function ___ide_onClick_a1bf40a39d004ff187865c6429f3624e_Callback() {}
    self.view.flxColour1.isVisible = true;
    self.view.flxColour2.isVisible = false;
    self.view.flxColour3.opacity = "0";
    self.view.flxTextAudioSurveyContent.animate(kony.ui.createAnimation({
        "100": {
            "left": "0%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "direction": kony.anim.DIRECTION_ALTERNATE,
        "duration": 0.4,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "iterationCount": 1
    }, {
        "animationEnd": ___ide_onClick_a1bf40a39d004ff187865c6429f3624e_Callback
    });
}