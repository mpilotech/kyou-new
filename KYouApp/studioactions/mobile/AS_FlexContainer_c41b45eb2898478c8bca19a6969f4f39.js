function AS_FlexContainer_c41b45eb2898478c8bca19a6969f4f39(eventobject) {
    function SHOW_ALERT_ide_onClick_e8ca897a39a04c24ad9aee344cef970a_True() {
        self.view.flxAttachImageForAudio.isVisible = false;
        self.view.flxImageUploadedAudio.isVisible = false;
        self.view.imgImageConfiemation.isVisible = true;
        self.view.lblAttachConfirmation.right = "20%";
        self.view.flxDeleteAttached.isVisible = true;
    }

    function SHOW_ALERT_ide_onClick_e8ca897a39a04c24ad9aee344cef970a_Callback() {
        SHOW_ALERT_ide_onClick_e8ca897a39a04c24ad9aee344cef970a_True();
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_INFO,
        "alertTitle": "Image Attached!!",
        "yesLabel": "Ok...Got it!",
        "message": "Your image file has been successfully attached.",
        "alertHandler": SHOW_ALERT_ide_onClick_e8ca897a39a04c24ad9aee344cef970a_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
    });
    undefined = undefined;
    undefined = undefined;
    undefined = undefined;
    undefined.show();
}