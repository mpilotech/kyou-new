function AS_FlexContainer_ae4c34d8432e4f2ba7e8bb2af2fad5a0(eventobject) {
    function SHOW_ALERT_ide_onClick_db455b1dcefe4e0286316998020b45ce_True() {
        self.view.flxAttachImage.isVisible = false;
        self.view.flxVidUploaded.isVisible = false;
        self.view.imgVidConfirm.isVisible = true;
        self.view.lblAttachConfirm.right = "18%";
        self.view.flxDeleteAttachment.isVisible = true;
    }

    function SHOW_ALERT_ide_onClick_db455b1dcefe4e0286316998020b45ce_Callback() {
        SHOW_ALERT_ide_onClick_db455b1dcefe4e0286316998020b45ce_True();
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_INFO,
        "alertTitle": "Video Attached!!",
        "yesLabel": "OK...Got it!",
        "message": "Your video has been successfully attached.",
        "alertHandler": SHOW_ALERT_ide_onClick_db455b1dcefe4e0286316998020b45ce_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
    });
    undefined.show();
}