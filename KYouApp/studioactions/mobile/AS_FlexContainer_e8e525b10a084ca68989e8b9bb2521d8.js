function AS_FlexContainer_e8e525b10a084ca68989e8b9bb2521d8(eventobject) {
    function SHOW_ALERT_ide_onClick_be691a9dda04430b8a4defd78a59ae9c_True() {
        self.view.flxAttachImageForAudio.isVisible = false;
        self.view.imgVidConfirmation.isVisible = true;
        self.view.imgImageConfiemation.isVisible = false;
        self.view.flxDeleteAttached.isVisible = true;
        self.view.lblAttachConfirmation.right = "18%";
        self.view.flxDeleteAttached.isVisible = true;
    }

    function SHOW_ALERT_ide_onClick_be691a9dda04430b8a4defd78a59ae9c_Callback() {
        SHOW_ALERT_ide_onClick_be691a9dda04430b8a4defd78a59ae9c_True();
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_INFO,
        "alertTitle": "Video Attached!!",
        "yesLabel": "OK...Got it!",
        "message": "Your video has been successfully attached.",
        "alertHandler": SHOW_ALERT_ide_onClick_be691a9dda04430b8a4defd78a59ae9c_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
    });
    undefined.show();
}