function AS_FlexContainer_e95a19f330ba4cc5b125cb6f579a344a(eventobject) {
    function SHOW_ALERT_ide_onClick_e5e77ecee253436da7d8df11d6d54ddd_True() {
        self.view.imgImageConfiem.isVisible = false;
        self.view.imgVidConfirm.isVisible = false;
        self.view.flxDeleteAttachment.isVisible = false;
        self.view.lblAttachConfirm.right = "35%";
    }

    function SHOW_ALERT_ide_onClick_e5e77ecee253436da7d8df11d6d54ddd_False() {}

    function SHOW_ALERT_ide_onClick_e5e77ecee253436da7d8df11d6d54ddd_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_e5e77ecee253436da7d8df11d6d54ddd_True();
        } else {
            SHOW_ALERT_ide_onClick_e5e77ecee253436da7d8df11d6d54ddd_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "DELETE ATTACHMENT",
        "yesLabel": "YES",
        "noLabel": "NO",
        "alertIcon": null,
        "message": "Your attachment will be removed. Do you want to continue?",
        "alertHandler": SHOW_ALERT_ide_onClick_e5e77ecee253436da7d8df11d6d54ddd_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
    });
}