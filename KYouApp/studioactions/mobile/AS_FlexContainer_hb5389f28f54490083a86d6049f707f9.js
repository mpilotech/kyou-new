function AS_FlexContainer_hb5389f28f54490083a86d6049f707f9(eventobject) {
    function ___ide_onClick_d3d0c7bad2d4446f904ab5d5ee5fe29a_Callback() {}
    self.view.flxColour1.isVisible = false;
    self.view.flxColour2.isVisible = true;
    self.view.flxColour3.opacity = "0";
    self.view.flxTextAudioSurveyContent.animate(kony.ui.createAnimation({
        "100": {
            "left": "-100%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "direction": kony.anim.DIRECTION_ALTERNATE,
        "duration": 0.4,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "iterationCount": 1
    }, {
        "animationEnd": ___ide_onClick_d3d0c7bad2d4446f904ab5d5ee5fe29a_Callback
    });
}