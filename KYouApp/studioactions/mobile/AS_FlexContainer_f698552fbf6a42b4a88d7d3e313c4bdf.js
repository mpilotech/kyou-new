function AS_FlexContainer_f698552fbf6a42b4a88d7d3e313c4bdf(eventobject, x, y) {
    function MOVE_ACTION____e2d5b1f250914e99b4033569f3b7c65d_Callback() {}

    function MOVE_ACTION____cd9bcb830e124247ab73ace0a750d559_Callback() {}
    self.view.Hamburger.animate(kony.ui.createAnimation({
        "100": {
            "left": "-100%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.4
    }, {
        "animationEnd": MOVE_ACTION____cd9bcb830e124247ab73ace0a750d559_Callback
    });
    self.view.flxContentMain.animate(kony.ui.createAnimation({
        "100": {
            "left": "0%",
            "stepConfig": {
                "timingFunction": kony.anim.EASE
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.4
    }, {
        "animationEnd": MOVE_ACTION____e2d5b1f250914e99b4033569f3b7c65d_Callback
    });
}