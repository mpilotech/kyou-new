function AS_FlexContainer_dc20bb16ce2b4af490b42c92cfab51f6(eventobject) {
    function SHOW_ALERT_ide_onClick_f300d55de69e438baf55a6b4eafcec2a_True() {
        self.view.flxAttachImage.isVisible = false;
        self.view.flxVidUploaded.isVisible = false;
        self.view.flxImageUploaded.isVisible = false;
        undefined.show();
    }

    function SHOW_ALERT_ide_onClick_f300d55de69e438baf55a6b4eafcec2a_False() {}

    function SHOW_ALERT_ide_onClick_f300d55de69e438baf55a6b4eafcec2a_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_f300d55de69e438baf55a6b4eafcec2a_True();
        } else {
            SHOW_ALERT_ide_onClick_f300d55de69e438baf55a6b4eafcec2a_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "CANCEL ATTACHMENT",
        "yesLabel": "YES",
        "noLabel": "NO",
        "alertIcon": null,
        "message": "Your Video will be deleted. Would you like to proceed?",
        "alertHandler": SHOW_ALERT_ide_onClick_f300d55de69e438baf55a6b4eafcec2a_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
    });
}