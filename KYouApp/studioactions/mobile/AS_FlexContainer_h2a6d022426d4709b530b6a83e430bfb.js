function AS_FlexContainer_h2a6d022426d4709b530b6a83e430bfb(eventobject) {
    function SHOW_ALERT_ide_onClick_bf15fedc866d438982c8fe455a70e23f_True() {}

    function SHOW_ALERT_ide_onClick_bf15fedc866d438982c8fe455a70e23f_False() {}

    function SHOW_ALERT_ide_onClick_bf15fedc866d438982c8fe455a70e23f_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_bf15fedc866d438982c8fe455a70e23f_True();
        } else {
            SHOW_ALERT_ide_onClick_bf15fedc866d438982c8fe455a70e23f_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "Cencel Message",
        "yesLabel": "Yes",
        "noLabel": "No",
        "message": "Your message will be canceled!!...Complete process?",
        "alertHandler": SHOW_ALERT_ide_onClick_bf15fedc866d438982c8fe455a70e23f_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
    });
}