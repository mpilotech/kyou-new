function AS_FlexContainer_id742e81688d40838d997d52885185e2(eventobject) {
    function SHOW_ALERT_ide_onClick_hd843cf056654842809fdfaba6c27cfb_True() {
        self.view.flxAttachImageForAudio.isVisible = false;
        self.view.flxImageUploadedAudio.isVisible = false;
    }

    function SHOW_ALERT_ide_onClick_hd843cf056654842809fdfaba6c27cfb_False() {}

    function SHOW_ALERT_ide_onClick_hd843cf056654842809fdfaba6c27cfb_Callback(response) {
        if (response === true) {
            SHOW_ALERT_ide_onClick_hd843cf056654842809fdfaba6c27cfb_True();
        } else {
            SHOW_ALERT_ide_onClick_hd843cf056654842809fdfaba6c27cfb_False();
        }
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": "Cancel Attachment",
        "yesLabel": "YES",
        "noLabel": "NO",
        "alertIcon": null,
        "message": "Your image will be deleted. Would you like to proceed?",
        "alertHandler": SHOW_ALERT_ide_onClick_hd843cf056654842809fdfaba6c27cfb_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_RIGHT
    });
}